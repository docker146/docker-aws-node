FROM docker:20

RUN apk add --no-cache curl \
					jq \
					python3 \
					py3-pip \
					git \
					nodejs \
					npm

RUN pip3 install awscli
